import 'package:flutter/material.dart';

class OutlineTextField extends StatelessWidget {

  final String hintText;
  final bool obscureText;
  final TextEditingController editController;
  final TextInputType keyboardType;

  OutlineTextField({this.hintText,this.obscureText,this.editController,this.keyboardType});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: editController,
      obscureText: this.obscureText,
      keyboardType: this.keyboardType,
      decoration: InputDecoration(
        hintText: this.hintText,
        enabledBorder: const OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          borderSide: const BorderSide(
              color: Color(0xFFdddddd), width: 1.2, style: BorderStyle.solid),
        ),
        disabledBorder: const OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          borderSide: const BorderSide(
              color: Color(0xFFdddddd), width: 0.3, style: BorderStyle.solid),
        ),
        focusedBorder: const OutlineInputBorder(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          borderSide: const BorderSide(
              color: Colors.greenAccent, width: 1.2, style: BorderStyle.solid),
        ),
      ),
    );
  }
}
