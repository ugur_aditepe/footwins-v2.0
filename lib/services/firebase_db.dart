import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:footwins/utility/fw_repsonse.dart';

abstract class BaseFWDb
{
  Future<FWResponse> setData(String collection,String document, Map<String, dynamic> data);

  Future<FWResponse> updateData(String collection,String document, Map<String, dynamic> data);

  Future<FWResponse> getData(String collection,String document);

  Future<FWResponse> deleteData(String collection,String document);

}

class FWDb implements  BaseFWDb {

  final Firestore _firestore = Firestore.instance;

  @override
  Future<FWResponse> setData(String collection, String document, Map<String, dynamic> data) async{
    var response = new FWResponse();   
    try 
    {
       if(document == "")
       {
          await _firestore.collection(collection).document().setData(data);
       }
       else
       {
          await _firestore.collection(collection).document(document).setData(data);
       }
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> updateData(String collection, String document, Map<String, dynamic> data) async{
    var response = new FWResponse();   
    try 
    {
       await _firestore.collection(collection).document(document).updateData(data);
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> getData(String collection, String document) async{
    var response = new FWResponse();   
    try 
    {
       var dbResponse = await _firestore.collection(collection).document(document).get();
       response.data = dbResponse;
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> deleteData(String collection, String document) async{
    var response = new FWResponse();   
    try 
    {
       await _firestore.collection(collection).document(document).delete();
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

}