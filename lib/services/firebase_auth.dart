import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:footwins/utility/fw_repsonse.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseFWAuth
{
  Future<FWResponse> signIn(String email, String password);

  Future<FWResponse> signUp(String email, String password);

  Future<FWResponse> signOut();

  Future<FWResponse> sendPasswordResetEmail(String email);

  Future<FWResponse> getCurrentUser();

}

class FWAuth implements  BaseFWAuth {

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Future<FWResponse> signIn(String email, String password) async {
    var response = new FWResponse();    
    if(email.trim() == "" || password.trim() == "")
    {
      response.status = false;
      response.errorCode = "A-001";
      response.errorMessage = "Eposta ve şifre bilgilerinizi girmelisiniz.";
      return response;
    }
    try 
    {
       var result = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
       response.data = result.user;
       var prefs = await SharedPreferences.getInstance();
       await prefs.setString('userId', result.user.uid);
       await prefs.setString('userEmail', result.user.email);
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> signUp(String email, String password) async {
    var response = new FWResponse();
    if(email.trim() == "" || password.trim() == "")
    {
      response.status = false;
      response.errorCode = "A-002";
      response.errorMessage = "Eposta ve şifre bilgilerinizi girmelisiniz.";
      return response;
    }    
    try 
    {
       var result = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
       response.data = result.user;
       var prefs = await SharedPreferences.getInstance();
       await prefs.setString('userId', result.user.uid);
       await prefs.setString('userEmail', result.user.email);
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> signOut() async {
    var response = new FWResponse();   
    try 
    {
       await _firebaseAuth.signOut();
       var prefs = await SharedPreferences.getInstance();
       await prefs.setString('userId', "");
       await prefs.setString('userEmail', "");
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> sendPasswordResetEmail(String email) async {
    var response = new FWResponse();   
    try 
    {
       await _firebaseAuth.sendPasswordResetEmail(email: email);
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }

  @override
  Future<FWResponse> getCurrentUser() async {
    var response = new FWResponse();   
    try 
    {
       var result =await _firebaseAuth.currentUser();
       response.data = result;
    } 
    catch (e) 
    {
      response.status = false;
      response.errorCode = e.code.toString();
      response.errorMessage = e.message.toString() + "{ " + e.code.toString() + " }";
    }
    return response;
  }


}