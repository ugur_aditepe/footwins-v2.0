import 'package:flutter/material.dart';

class FWUtility
{

    static SnackBar getSnackBar(String content)
    {
        var snack = new SnackBar(
          content: new Text(content),
          action: null,
          duration: new Duration(seconds: 4),
          backgroundColor: Colors.black,
        );
        return snack;
    }

}