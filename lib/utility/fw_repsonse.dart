class FWResponse
{
  bool status;
  String errorCode;
  String errorMessage;
  dynamic data;

  FWResponse()
  {
    this.status = true;
    this.errorCode = "";
    this.errorMessage = "";
  }

}