import 'package:flutter/material.dart';
import 'package:footwins/pages/login.dart';
import 'pages/splash.dart';

void main() {
  runApp(new MaterialApp(
    initialRoute: "/",
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.white,
      primaryColorBrightness: Brightness.light,
      primaryColorDark: Colors.white,
    ),
    routes: {
       "/": (context) => SplashPage(),
       "/login": (context) => Login(),
    },
  ));
}
