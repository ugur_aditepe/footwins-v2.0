import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MatchsTop extends StatefulWidget {
  @override
  _MatchsTopState createState() => _MatchsTopState();
}

class _MatchsTopState extends State<MatchsTop> {
  int totalPoint = 0;
  int totalCount = 0;

  @override
  void initState() {
    List<String> totalCList = new List<String>();
    int totalP = 0;
    Firestore.instance
        .collection("userApproveMatchs")
        .snapshots()
        .listen((onData) {
      setState(() {
        totalP = 0;
        totalCList = new List<String>();
      });

      for (var item in onData.documents) {
        totalP = totalP + (10 * int.parse(item.data["misli"].toString()));
        if(!totalCList.contains(item.data["userId"].toString()))
           totalCList.add(item.data["userId"].toString());
      }
      setState(() {
        totalPoint = totalP;
        totalCount = totalCList.length;
      });

      super.initState();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      padding: EdgeInsets.all(15),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.only(right: 7),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    stops: [0.1, 0.4, 0.7, 0.9],
                    colors: [
                      Colors.indigo[800],
                      Colors.indigo[500],
                      Colors.indigo[300],
                      Colors.indigo[200],
                    ],
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 7,
                      blurRadius: 3,
                      offset: Offset(0, 7),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 17),
                      child: Center(
                        child: Text(
                          "Toplam Kasa",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 22),
                      child: Center(
                        child: Text(
                          totalPoint.toString() + " PUAN",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 25),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: EdgeInsets.only(left: 7),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.1, 0.4, 0.7, 0.9],
                    colors: [
                      Colors.red[800],
                      Colors.red[500],
                      Colors.red[300],
                      Colors.red[200],
                    ],
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 7,
                      blurRadius: 3,
                      offset: Offset(0, 7),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 17),
                      child: Center(
                        child: Text(
                          "Toplam Oynayan",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 22),
                      child: Center(
                        child: Text(
                          totalCount.toString() + " Kişi",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 25),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
