import 'package:flutter/material.dart';
import 'package:footwins/pages/admin/admin_home.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/Enums.dart';
import 'package:footwins/pages/login.dart';
import 'package:footwins/pages/home.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  FWAuth fwAuth = new FWAuth();
  FWDb fwDb = new FWDb();
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    splashAsync();
    super.initState();
  }

  void splashAsync() async
  {
    var response = await fwAuth.getCurrentUser();
    if(response.data == null || response.data.uid == null)
    {
      setState(() {
        authStatus = AuthStatus.NOT_LOGGED_IN;
      });
    }
    else
    {
      var userResponse = await fwDb.getData("users", response.data.uid.toString());
      if(!userResponse.status)
      {
        setState(() {
          authStatus = AuthStatus.NOT_LOGGED_IN;
        });
      }
      else
      {
        if(userResponse.data.data["isAdmin"] == "1")
        {
           setState(() {
              authStatus = AuthStatus.LOGGED_IN_ADMIN;
           });
        }
        else
        {
           setState(() {
              authStatus = AuthStatus.LOGGED_IN_USER;
           });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return Login();
      case AuthStatus.LOGGED_IN_USER:
        return Home();
      case AuthStatus.LOGGED_IN_ADMIN:
        return AdminHome();
      default:
        return buildWaitingScreen();
    }
  }

  Widget buildWaitingScreen() {
    return Scaffold(
      key: scaffoldKey,
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
