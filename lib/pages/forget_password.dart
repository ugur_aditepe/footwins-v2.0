import 'package:flutter/material.dart';
import 'package:footwins/component/outline_textfield.dart';
import 'package:footwins/component/raund_button.dart';
import 'package:footwins/pages/login.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  FWAuth fwAuth = new FWAuth();

  bool isLoading = false;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final emailCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  doForgetPassord() async{
    setState(() {isLoading = true;});
    var response = await fwAuth.sendPasswordResetEmail(emailCtrl.text);
    setState(() {isLoading = false;});
    if(!response.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(response.errorMessage));
      return;
    }
    scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Şifre değiştirme bağlantınız email adresinize gönderildi."));
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
        isLoading: isLoading,
        child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            title: GestureDetector(
              child: Icon(Icons.arrow_back),
              onTap: () => Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return Login();
              })),
            ),
            elevation: 0,
          ),
          body: Center(
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.only(top: 100.0, bottom: 50.0),
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 128.0,
                              child: Padding(
                                padding: EdgeInsets.only(left: 70, right: 70),
                                child: Center(
                                  child: Image(
                                    image: AssetImage("assets/logo.png"),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 5),
                              child: Text(
                                "Şifremi Unuttum",
                                style: TextStyle(
                                    color: Color(0xff373737),
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 25, right: 25, top: 5, bottom: 5),
                              child: Center(
                                child: Text(
                                  "Yeni şifre alabilmek için lütfen sistemizde kayıtlı email adresini giriniz.Sonrasında email adresinize şifre sıfırlama maili yönlendirilecektir.",
                                  style: TextStyle(
                                    color: Color(0xff717171),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                      child: new OutlineTextField(
                        hintText: "Email Adresiniz",
                        obscureText: false,
                        editController: emailCtrl,
                        keyboardType: TextInputType.emailAddress,
                      ),
                    ),
                    new Padding(
                      padding: EdgeInsets.only(
                          left: 25, right: 25, bottom: 10, top: 5),
                      child: RoundButton(
                        backgroundColor: Color(0xffB91F1D),
                        buttonText: Text(
                          "Şifreyi Gönder",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        ),
                        textColor: Colors.teal,
                        onPressed: doForgetPassord,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
