import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:footwins/pages/match_card.dart';
import 'package:footwins/pages/matchs_top.dart';


class Matchs extends StatefulWidget {
  @override
  _MatchsState createState() => _MatchsState();
}

class _MatchsState extends State<Matchs> {

  List<DocumentSnapshot> list = new List<DocumentSnapshot>();

  @override
  void initState() {
    Firestore.instance.collection('matchs').where("status", isEqualTo: "1").snapshots().listen((onData){
        setState(() {
          list = onData.documents;
        });
        super.initState();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        MatchsTop(),
        Padding(
          padding: EdgeInsets.only(top: 5),
        ),
        Expanded(
          child: new ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              MatchCard(
                document: list[index],
              ),
          itemCount: list.length,
        ),
        ),
      ],
    );
  }
}
