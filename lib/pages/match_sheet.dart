import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home.dart';

class MatchSheet extends StatefulWidget {
  final HomeState homeState;
  MatchSheet({this.homeState});

  @override
  _MatchSheetState createState() => _MatchSheetState();
}

class _MatchSheetState extends State<MatchSheet> {

  FWAuth fwAuth = new FWAuth();
  FWDb fwDb = new FWDb();

  List<Map<String, dynamic>> list = new List<Map<String, dynamic>>();
  bool isLoading = true;
  List<int> misliList = List<int>();
  int currentMisli = 1;

  void matchStateAsync() async
  {
     setState(() {
       isLoading = true;
     });

     SharedPreferences prefs = await SharedPreferences.getInstance();
     String userId = prefs.getString("userId");

     var matchs = await Firestore.instance.collection("userMatchs").getDocuments();

     for(var itemMatch in matchs.documents)
     {
         Map<String, dynamic> mapList = new Map();
         mapList.addAll(itemMatch.data);
         mapList["matchId"] = itemMatch.documentID.toString();
 
         var userMatchs = await Firestore.instance.collection("userMatchs").document(userId + "__" + itemMatch.documentID.toString()).get();
         mapList["homeOrAwayStatus"] = userMatchs.data["homeOrAway"];
         if (userMatchs.data["homeOrAway"] == "1")
            mapList["homeOrAway"] = "ev sahibi";
         if (userMatchs.data["homeOrAway"] == "0")
            mapList["homeOrAway"] = "beraberlik";
         if (userMatchs.data["homeOrAway"] == "2")
            mapList["homeOrAway"] = "deplasman";
        
         setState(() {
            list.add(mapList);
         });
     }
     setState(() {
       isLoading = false;
     });
  }



  @override
  void initState() {
    for (var i = 1; i < 31; i++) {
      misliList.add(i);
    }
    matchStateAsync();
    super.initState();
  }
  
  doMatch() async{
     setState(() {isLoading = true;});
     SharedPreferences prefs = await SharedPreferences.getInstance();
     String userId = prefs.getString("userId");

     var userResponse = await fwDb.getData("users", userId);
     if(!userResponse.status)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
        return;
     }
     int currentBalance = int.parse(userResponse.data["balance"].toString());     
     if(currentBalance < 10*currentMisli)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Tahmin yapabilmek için yeterli puanınız bulunmamaktadır.Tahmin yapabilmek için paket satın alabililirsiniz."));
        return;
     }
     var approveMatchsResponse = await Firestore.instance.collection("userApproveMatchs").where("userId", isEqualTo:userId).getDocuments();
     if(approveMatchsResponse != null && approveMatchsResponse.documents.length == 3)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Aynı gün için en fazla 3 tahmin hakkınız bulunmaktadır."));
        return;
     }

     for (var itemUserMatch in list) {
       var userMatchResponse = await fwDb.deleteData("userMatchs", userId + "__" + itemUserMatch["matchId"].toString());
       if(!userMatchResponse.status)
       {
            widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
            return;
       }
     }

     var userApproveMatchResponse = await fwDb.setData("userApproveMatchs", "", {'userId': userId, 'data': list , 'misli' : currentMisli});
     if(!userApproveMatchResponse.status)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
        return;
     }

     var userUpdResponse = await fwDb.updateData("users", userId, {'balance': (currentBalance- (10*currentMisli))});
     if(!userUpdResponse.status)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
        return;
     }

     var now = new DateTime.now();
     var feedResponse = await fwDb.setData('feeds', "", { 'key': userId, 'type': 'USER_GUESS','name': userResponse.data["name"].trim(), 'text' : 'yeni bir tahminde bulundu.','date' : now });
     if(!feedResponse.status)
     {
        widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
        return;
     }
     setState(() {isLoading = true;});
     widget.homeState.scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Tahminleriniz alındı.Tahminlerinizi tahminlerim menüsünden kontrol edebilirsiniz."));
  }


  void changedDropDownItem(int selectedMisli) {
    setState(() {
      currentMisli = selectedMisli;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: LoadingOverlay(
        isLoading: isLoading,
        child: Column(
          children: <Widget>[
            Container(
              alignment: AlignmentDirectional.centerStart,
              child: Padding(
                padding: EdgeInsets.only(top: 10, left: 10),
                child: Text("Tahminlerin"),
              ),
            ),
            Divider(),
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) => Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: <Widget>[
                            new SizedBox(
                              width: 30,
                              child: CachedNetworkImage(
                                imageUrl: list[index]['homeLogo'],
                                fit: BoxFit.contain,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              height: 30,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10),
                              child: Text(list[index]['homeName']),
                            ),
                          ],
                        ),
                      ),
                      flex: 5,
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          list[index]['homeOrAway'],
                          style: TextStyle(
                              fontSize: 11,
                              color: Colors.indigo[300],
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      flex: 2,
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Text(list[index]['awayName']),
                            ),
                            new SizedBox(
                              width: 30,
                              child: CachedNetworkImage(
                                imageUrl: list[index]['awayLogo'],
                                fit: BoxFit.contain,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              height: 30,
                            ),
                          ],
                        ),
                      ),
                      flex: 5,
                    ),
                  ],
                ),
                itemCount: list.length,
              ),
            ),
            Visibility(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Container(
                    alignment: AlignmentDirectional.centerStart,
                    padding: EdgeInsets.only(left: 10),
                    child: Row(
                      children: <Widget>[
                        Text("Şansını katla : "),
                        Container(
                          width: 60,
                          child: new DropdownButton<int>(
                            value: currentMisli,
                            elevation: 0,
                            items: misliList.map((int value) {
                              return new DropdownMenuItem<int>(
                                value: value,
                                child: new Text(value.toString()),
                              );
                            }).toList(),
                            onChanged: changedDropDownItem,
                          ),
                        )
                      ],
                    ),
                  )),
                  Expanded(
                      child: Container(
                    alignment: AlignmentDirectional.centerEnd,
                    child: Container(
                        padding: EdgeInsets.only(right: 10),
                        alignment: AlignmentDirectional.centerEnd,
                        child: OutlineButton(
                            onPressed: doMatch, child: Text("Tahmin yap"))),
                  ))
                ],
              ),
              visible: list.length == 3,
            ),
          ],
        ),
      ),
    );
  }
}
