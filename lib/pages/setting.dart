import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:footwins/pages/change_password.dart';
import 'package:footwins/pages/login.dart';
import 'package:footwins/pages/messages.dart';
import 'package:footwins/pages/update_profile.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/utility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  FWDb fwDb = new FWDb();
  FWAuth fwAuth = new FWAuth();

  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool notification = false;

  @override
  void initState() {
    settingAsync();
    super.initState();
  }

  void settingAsync() async{
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String userId = prefs.getString("userId");
      Firestore.instance.collection("users").document(userId).snapshots().listen((data){
           if(data != null)
           {
              bool notif = data.data["notification"] != null ? data.data["notification"]  : false;
              setState(() {notification = notif;});
           }
      });
  }

  doUpdateNotification(bool notification) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userId");
    var responseDb = await fwDb.updateData('users', userId, { 'notification': notification});
    if(!responseDb.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(responseDb.errorMessage));
      return;
    }
    else
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Bildirim tercihiniz başarıyla güncellendi."));
      return;
    }
  }

  doLogout() async{
      var response = await fwAuth.signOut();
      if(!response.status)
      {
        scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(response.errorMessage));
        return;
      }
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (BuildContext context) {
          return Login();
      }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Ayarlar",
          style: TextStyle(color: Color(0xff373737)),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.3,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          alignment: AlignmentDirectional.topStart,
          child: Padding(
            padding: EdgeInsets.all(25),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Hesap Detayları",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff373737),
                        fontSize: 16),
                    textAlign: TextAlign.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Profil düzenle"),
                    subtitle:
                        Text('Kullanıcı bilgilerinizi güncelleyebilirsiniz'),
                    leading: Icon(Icons.account_circle),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      return UpdateProfile();
                    })),
                    dense: true,
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Şifre değiştir"),
                    subtitle: Text('Şifrenizi değiştirebilirsiniz'),
                    leading: Icon(Icons.lock_outline),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      return ChangePassword();
                    })),
                    dense: true,
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Mesajlar"),
                    subtitle:
                        Text('Mesajlarınızı görüntüleyebilirsiniz'),
                    leading: Icon(Icons.message),
                    trailing: Icon(Icons.chevron_right),
                    onTap: () => Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      return Messages();
                    })),
                    dense: true,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Text(
                    "Paket Detayları",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff373737),
                        fontSize: 16),
                    textAlign: TextAlign.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Paket satın al"),
                    subtitle: Text(
                        'Tahmin yapmak için yeni paket satın alabilirsiniz'),
                    leading: Icon(Icons.attach_money),
                    trailing: Icon(Icons.chevron_right),
                    dense: true,
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Puan Bozdur"),
                    subtitle: Text('Kazandığınız puanları kolayca bozdurabilirsiniz'),
                    leading: Icon(Icons.money_off),
                    trailing: Icon(Icons.chevron_right),
                    dense: true,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Text(
                    "Sistem Detayları",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff373737),
                        fontSize: 16),
                    textAlign: TextAlign.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                  ),
                  ListTile(
                    contentPadding: EdgeInsets.all(0),
                    title: Text("Bildirim ayarları"),
                    subtitle: Text('Bildirim ayarlarınızı değiştirebilirsiniz'),
                    leading: Icon(Icons.notifications),
                    trailing: CupertinoSwitch(
                      onChanged: doUpdateNotification,
                      value: notification,
                    ),
                    dense: true,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                    child: Divider(
                      height: 2,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: Center(
                      child: GestureDetector(
                        onTap: doLogout,
                        child: Text(
                          "Uygulamadan Çıkış Yap",
                          style: TextStyle(color: Colors.redAccent),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
