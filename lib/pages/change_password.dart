import 'package:flutter/material.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  FWAuth fwAuth = new FWAuth();

  bool isLoading = false;
  String userId;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController emailCont = new TextEditingController();

  @override
  void initState() {
    changePasswordAsync();
    super.initState();
  }

  void changePasswordAsync() async
  {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String email = prefs.getString("userEmail");
      setState(() {
         emailCont.text = email;
      });
  }

  doForgetPassword() async{
    setState(() {isLoading = true;});
    var response = await fwAuth.sendPasswordResetEmail(emailCont.text);
    setState(() {isLoading = false;});
    if(!response.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(response.errorMessage));
      return;
    }
    scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Şifre değiştirme bağlantınız email adresinize gönderildi."));
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Şifre Değiştir",
            style: TextStyle(color: Color(0xff373737)),
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0.3,
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          alignment: AlignmentDirectional.topStart,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                  ),
                  Center(
                    child: Text(
                      "Şifrenizi değiştirmek için email adresiniz aşağıdadır.Şifre değiştirme bağlantınız email adresinize gönderilecektir.",
                      style: TextStyle(color: Color(0xff373737), fontSize: 13),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "Email Adresi",
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextField(
                          textAlign: TextAlign.end,
                          enabled: false,
                          controller: emailCont,
                          style: TextStyle(
                            color: Color(0xff717171),
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: GestureDetector(
                      child: Center(
                      child: Text(
                        "Bağlantıyı Gönder",
                        style: TextStyle(color: Colors.redAccent),
                      ),
                    ),
                    onTap: doForgetPassword,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
