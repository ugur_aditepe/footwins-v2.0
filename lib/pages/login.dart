import 'package:flutter/material.dart';
import 'package:footwins/component/outline_textfield.dart';
import 'package:footwins/component/raund_button.dart';
import 'package:footwins/pages/admin/admin_home.dart';
import 'package:footwins/pages/forget_password.dart';
import 'package:footwins/pages/home.dart';
import 'package:footwins/pages/signup.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isLoading = false;
  final emailCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  FWAuth fwAuth = new FWAuth();
  FWDb fwDb = new FWDb();

  @override
  void initState() {
    super.initState();
  }
  
  void doLogin() async
  {
    setState(() {isLoading = true;});
    var response = await fwAuth.signIn(emailCtrl.text, passwordCtrl.text);
    setState(() {isLoading = false;});
    if(!response.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(response.errorMessage));
      return;
    }

    var userResponse = await fwDb.getData("users", response.data.uid.toString());
    if(!userResponse.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(userResponse.errorMessage));
      return;
    }

    if(userResponse.data.data["isAdmin"] == "1")
    {
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (BuildContext context) {
            return AdminHome();
      }));
    }
    else
    {
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (BuildContext context) {
            return Home();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: LoadingOverlay(
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 150.0, bottom: 50.0),
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 128.0,
                            child: Padding(
                              padding: EdgeInsets.only(left: 70, right: 70),
                              child: Center(
                                child: Image(
                                  image: AssetImage("assets/logo.png"),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15, bottom: 5),
                            child: Text(
                              "Giriş Yap",
                              style: TextStyle(
                                  color: Color(0xff373737),
                                  fontSize: 25,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: Text(
                              "Uygulamaya devam edebilmek için giriş yapınız",
                              style: TextStyle(
                                color: Color(0xff717171),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                    child: new OutlineTextField(
                      hintText: "Email Adresiniz",
                      obscureText: false,
                      editController: emailCtrl,
                      keyboardType: TextInputType.emailAddress,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                    child: new OutlineTextField(
                      hintText: "Şifreniz",
                      obscureText: true,
                      editController: passwordCtrl,
                      keyboardType: TextInputType.visiblePassword,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, bottom: 10, top: 5),
                    child: Container(
                      alignment: AlignmentDirectional.centerEnd,
                      child: GestureDetector(
                        onTap: () => Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return ForgetPassword();
                        })),
                        child: new Text(
                          "Şifremi unuttum",
                          style: TextStyle(
                              color: Color(0xff717171),
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, bottom: 10, top: 5),
                    child: RoundButton(
                      backgroundColor: Color(0xff111f5e),
                      buttonText: Text(
                        "Giriş Yap",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w600),
                      ),
                      textColor: Colors.teal,
                      onPressed: doLogin,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, bottom: 10, top: 10),
                    child: Center(
                      child: GestureDetector(
                        onTap: () => Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return SignUp();
                        })),
                        child: new Text("Henüz hesabınız yok mu? Kayıt Ol!",
                            style: TextStyle(
                                color: Color(0xff717171),
                                fontWeight: FontWeight.w400)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        isLoading: isLoading,
        opacity: 0.3,
        color: Colors.white,
        progressIndicator: const CircularProgressIndicator(),
      ),
    );
  }
}
