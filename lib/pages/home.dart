import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:footwins/pages/feeds.dart';
import 'package:footwins/pages/match_sheet.dart';
import 'package:footwins/pages/matchs.dart';
import 'package:footwins/pages/notification.dart';
import 'package:footwins/pages/setting.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  bool fabVisible = false;
  String userId;
  final scaffoldKey = GlobalKey<ScaffoldState>();
  var macthSheet;

  @override
  void initState() {
    homeAsync();
    super.initState();
  }

  void homeAsync() async
  {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String userId = prefs.getString("userId");
      Firestore.instance.collection("userMatchs").where("userId", isEqualTo: userId).snapshots().listen((data){
           if(data.documents.length > 0)
           {
              setState(() {fabVisible = true;});
           }
           else
           {
              setState(() {fabVisible = false;});
           }
      });
  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        key: scaffoldKey,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: Visibility(
          child: FloatingActionButton(
            onPressed: () {
              showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return MatchSheet(
                      homeState: this,
                    );
                  });
            },
            backgroundColor: Color(0xffB91F1D),
            child: Icon(Icons.list),
          ),
          visible: fabVisible,
        ),
        appBar: AppBar(
          elevation: 0.5,
          title: Image(
            height: 37,
            image: AssetImage("assets/logohome.png"),
          ),
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NotificationPage())),
              color: Color(0xff373737),
              iconSize: 28,
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SettingPage())),
              color: Color(0xff373737),
              iconSize: 28,
            )
          ],
          bottom: TabBar(
            indicatorPadding: EdgeInsets.only(left: 15, right: 15),
            indicatorWeight: 4,
            indicatorColor: Color(0xffB91F1D),
            labelColor: Color(0xff373737),
            tabs: <Widget>[
              new Tab(
                child: Text(
                  "MAÇLAR",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ),
              new Tab(
                child: Text(
                  "AKIŞ",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.grey.shade300,
              child: Matchs(),
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.grey.shade300,
              child: Feeds(),
            ),
          ],
        ),
      ),
    );
  }
}
