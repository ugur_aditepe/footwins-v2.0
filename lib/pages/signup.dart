import 'package:flutter/material.dart';
import 'package:footwins/component/outline_textfield.dart';
import 'package:footwins/component/raund_button.dart';
import 'package:footwins/pages/login.dart';
import 'package:footwins/services/firebase_auth.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'home.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isLoading = false;
  final emailCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();
  final nameCtrl = TextEditingController();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  FWAuth fwAuth = new FWAuth();
  FWDb fwDb = new FWDb();

  @override
  void initState() {
    super.initState();
  }

  goLogin() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) {
          return Login();
    }));
  }

  doSignUp() async
  {
      setState(() {isLoading = true;});
      var response = await fwAuth.signUp(emailCtrl.text, passwordCtrl.text);
      if(!response.status)
      {
        setState(() {isLoading = false;});
        scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(response.errorMessage));
        return;
      }
      var responseDb = await fwDb.setData('users', response.data.uid, { 'userId': response.data.uid, 'name': nameCtrl.text.trim(), 'balance' : 0 });
      if(!responseDb.status)
      {
        setState(() {isLoading = false;});
        scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(responseDb.errorMessage));
        return;
      }
      var now = new DateTime.now();
      responseDb = await fwDb.setData('feeds', "", { 'key': response.data.uid, 'type': 'USER_CREATE','name': nameCtrl.text.trim(), 'text' : 'footwins platformuna katıldı.','date' : now });
      if(!responseDb.status)
      {
        setState(() {isLoading = false;});
        scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(responseDb.errorMessage));
        return;
      }
      setState(() {isLoading = false;});
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (BuildContext context) {
          return Home();
      }));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: LoadingOverlay(
        isLoading: isLoading,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 150.0, bottom: 50.0),
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 128.0,
                            child: Padding(
                              padding: EdgeInsets.only(left: 70, right: 70),
                              child: Center(
                                child: Image(
                                  image: AssetImage("assets/logo.png"),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15, bottom: 5),
                            child: Text(
                              "Kayıt Ol",
                              style: TextStyle(
                                  color: Color(0xff373737),
                                  fontSize: 25,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 5, bottom: 5),
                            child: Text(
                              "Uygulamayı kullanabilmek için kayıt olunuz",
                              style: TextStyle(
                                color: Color(0xff717171),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                    child: new OutlineTextField(
                      hintText: "İsim Soyisim",
                      obscureText: false,
                      editController: nameCtrl,
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                    child: new OutlineTextField(
                      hintText: "Email Adresiniz",
                      obscureText: false,
                      editController: emailCtrl,
                      keyboardType: TextInputType.emailAddress,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(left: 25, right: 25, bottom: 10),
                    child: new OutlineTextField(
                      hintText: "Şifreniz",
                      obscureText: true,
                      editController: passwordCtrl,
                      keyboardType: TextInputType.visiblePassword,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, bottom: 10, top: 5),
                    child: RoundButton(
                      backgroundColor: Color(0xff111f5e),
                      buttonText: Text(
                        "Kayıt Ol",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w600),
                      ),
                      textColor: Colors.teal,
                      onPressed: doSignUp,
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, bottom: 10, top: 10),
                    child: Center(
                      child: GestureDetector(
                        onTap: ()=> Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                              return Login();
                        })),
                        child: new Text("Zaten hesabınız var mı? Giriş Yap!",
                            style: TextStyle(
                                color: Color(0xff717171),
                                fontWeight: FontWeight.w400)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
