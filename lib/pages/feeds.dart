import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Feeds extends StatefulWidget {
  @override
  _FeedsState createState() => _FeedsState();
}

class _FeedsState extends State<Feeds> {
  List<DocumentSnapshot> feeds = [];
  bool isLoading = false;
  bool hasMore = true;
  int documentLimit = 10;
  DocumentSnapshot lastDocument;
  ScrollController _scrollController = ScrollController();
  Firestore firestore = Firestore.instance;

  List<Color> randColor = [
    Colors.redAccent,
    Colors.blueAccent,
    Colors.amberAccent,
    Colors.deepOrangeAccent,
    Colors.indigoAccent,
    Colors.deepPurpleAccent,
    Colors.lightGreenAccent,
    Colors.limeAccent,
    Colors.tealAccent
  ];

  Color getRandColor() {
    int rnd = Random.secure().nextInt(8);
    return randColor[rnd];
  }

  getFeeds() async {
    if (!hasMore) {
      return;
    }
    if (isLoading) {
      return;
    }
    setState(() {
      isLoading = true;
    });
    QuerySnapshot querySnapshot;
    if (lastDocument == null) {
      querySnapshot = await firestore
          .collection('feeds')
          .limit(documentLimit)
          .orderBy('date', descending: true)
          .getDocuments();
    } else {
      querySnapshot = await firestore
          .collection('feeds')
          .startAfterDocument(lastDocument)
          .limit(documentLimit)
          .orderBy('date', descending: true)
          .getDocuments();
    }
    if (querySnapshot.documents.length < documentLimit) {
      hasMore = false;
    }
    if(querySnapshot.documents.length > 0)
      lastDocument = querySnapshot.documents[querySnapshot.documents.length - 1];
    feeds.addAll(querySnapshot.documents);
    setState(() {
      isLoading = false;
    });
  }

  getFeedsColor(String feedType)
  {
      if(feedType == "USER_GUESS")
        return Colors.deepPurpleAccent;
      else if(feedType == "USER_CREATE")
        return Colors.lightBlue[300];
  }

  @override
  void initState() {
    getFeeds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currentScroll = _scrollController.position.pixels;
      double delta = MediaQuery.of(context).size.height * 0.20;
      if (maxScroll - currentScroll <= delta) {
        getFeeds();
      }
    });

    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            color: Colors.white,
            child: feeds.length == 0
                ? Center(
                    child: Text('Şuanlık gösterilecek birşey yok...'),
                  )
                : ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      height: 1,
                    ),
                    controller: _scrollController,
                    itemCount: feeds.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        contentPadding: EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        leading: CircleAvatar(
                          backgroundColor: getFeedsColor(feeds[index].data['type'].toString()),
                          child: Center(
                            child: Text(feeds[index].data['user']),
                          ),
                        ),
                        title: Text(feeds[index].data['text']),
                      );
                    },
                  ),
          ),
        ),
        isLoading
            ? Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: Center(
                    child: Container(
                      color: Colors.white,
                      width: 30,
                      height: 30,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
              )
            : Container()
      ],
    );
  }
}
