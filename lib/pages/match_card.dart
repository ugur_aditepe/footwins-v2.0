import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MatchCard extends StatefulWidget {
  final DocumentSnapshot document;
  MatchCard({this.document});

  @override
  _MatchCardState createState() => _MatchCardState();
}

class _MatchCardState extends State<MatchCard> {

  FWDb fwDb = new FWDb();
  
  String homeOrAway = "";
  String userId = "";
  Color homeColor = Colors.white;
  Color defColor = Colors.white;
  Color awayColor = Colors.white;
  Color homeTextColor = Colors.black;
  Color defTextColor = Colors.black;
  Color awayTextColor = Colors.black;

  @override
  void initState() {
      matchCardAsync();
      super.initState();
  }

  void setDefaultColor()
  {
    setState(() {
      homeColor = Colors.white;
      defColor = Colors.white;
      awayColor = Colors.white;
      homeTextColor = Colors.black;
      defTextColor = Colors.black;
      awayTextColor = Colors.black;
    });
  }

  void matchCardAsync() async
  {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String userId = prefs.getString("userId");

      Firestore.instance.collection("userMatchs").where("userId", isEqualTo: userId).where("matchId", isEqualTo: widget.document.documentID).snapshots().listen((data){
         if(data.documents.length == 0)
         {
            setDefaultColor();
         }
         else
         {
            setDefaultColor();
            var homeOrAway = data.documents.first["homeOrAway"];
            setState(() {
              if (homeOrAway == "1") {
                homeColor = Colors.indigo[300];
                homeTextColor = Colors.white;
              }
              if (homeOrAway == "0") {
                defColor = Colors.indigo[300];
                defTextColor = Colors.white;
              }
              if (homeOrAway == "2") {
                awayColor = Colors.indigo[300];
                awayTextColor = Colors.white;
              }
            });
         }
      });

  }

  doAddMatch(String homeOrAway) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userId");
    await fwDb.setData('userMatchs', userId + "__" + widget.document.documentID.toString(), { 'matchId': widget.document.documentID.toString(), 'userId': userId, 'homeOrAway' : homeOrAway });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.all(Radius.circular(0))),
      child: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: Color(0xfff4f5f7),
                borderRadius: new BorderRadius.only(
                    topLeft: Radius.circular(7), topRight: Radius.circular(7))),
            padding: EdgeInsets.all(15),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Text(
                    widget.document['leaqueName'].toString(),
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Color(0xff373737)),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    alignment: AlignmentDirectional.centerEnd,
                    child: Text(
                      widget.document['time'],
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xffB91F1D)),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 130,
            padding: EdgeInsets.only(
              left: 15,
              right: 15,
            ),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Container(
                            alignment: AlignmentDirectional.center,
                            child: SizedBox(
                              child: CachedNetworkImage(
                                imageUrl: widget.document['homeLogo'],
                                fit: BoxFit.contain,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              height: 35,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 8,
                        child: Text(
                          widget.document['homeName'],
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "0",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: Container(
                            alignment: AlignmentDirectional.center,
                            child: SizedBox(
                              child: CachedNetworkImage(
                                imageUrl: widget.document['awayLogo'],
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                              height: 35,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 8,
                        child: Text(
                          widget.document['awayName'],
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(
                          "0",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: 50,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () => doAddMatch("1"),
                    child: Container(
                      decoration: BoxDecoration(
                        color: homeColor,
                        border: Border(
                          top: BorderSide(color: Color(0xffeeeeee), width: 1),
                          right: BorderSide(color: Color(0xffeeeeee), width: 1),
                        ),
                      ),
                      child: Center(
                        child: Text("ev sahibi",
                            style: TextStyle(color: homeTextColor)),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () => doAddMatch("0"),
                    child: Container(
                      decoration: BoxDecoration(
                        color: defColor,
                        border: Border(
                          top: BorderSide(color: Color(0xffeeeeee), width: 1),
                          right: BorderSide(color: Color(0xffeeeeee), width: 1),
                        ),
                      ),
                      child: Center(
                        child: Text("beraberlik",
                            style: TextStyle(color: defTextColor)),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () => doAddMatch("2"),
                    child: Container(
                      decoration: BoxDecoration(
                        color: awayColor,
                        border: Border(
                          top: BorderSide(color: Color(0xffeeeeee), width: 1),
                          right: BorderSide(color: Color(0xffeeeeee), width: 1),
                        ),
                      ),
                      child: Center(
                        child: Text("deplasman",
                            style: TextStyle(color: awayTextColor)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
