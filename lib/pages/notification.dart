import 'package:flutter/material.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Bildirimler",
          style: TextStyle(color: Color(0xff373737)),
        ),
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 0.3,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        alignment: AlignmentDirectional.center,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 150,
                width: 150,
                child: Icon(
                  Icons.notifications_none,
                  size: 120,
                  color: Colors.redAccent,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25, bottom: 10),
                child: Text(
                  "Bildirimin yok!",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color(0xff373737)),
                ),
              ),
              Padding(
                padding:
                    EdgeInsets.only(top: 10, bottom: 25, left: 25, right: 25),
                child: Text(
                  "Henüz bir bildirimin yok.Bildirimin olduğunda buradan kontrol edebilirsin.",
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
