import 'package:flutter/material.dart';
import 'package:footwins/services/firebase_db.dart';
import 'package:footwins/utility/utility.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {

  FWDb fwDb = new FWDb();

  bool isLoading = false;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController nameCont = new TextEditingController();
  TextEditingController emailCont = new TextEditingController();
  TextEditingController phoneCont = new TextEditingController();

  @override
  void initState() {
    updateProfileAsync();
    super.initState();
  }

  void updateProfileAsync() async
  {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String userId = prefs.getString("userId");
      String email = prefs.getString("userEmail");
      var responseDb = await fwDb.getData('users', userId);
      if(!responseDb.status)
      {
        scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(responseDb.errorMessage));
        return;
      }
      setState(() {
         nameCont.text = responseDb.data["name"].toString();
         emailCont.text = email;
         phoneCont.text = responseDb.data["phone"] != null
              ? responseDb.data["phone"].toString()
              : "";
      });
  }

  doUpdateUser() async{
    setState(() {isLoading = true;});
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userId = prefs.getString("userId");
    var responseDb = await fwDb.updateData('users', userId, {'name': nameCont.text, 'phone': phoneCont.text});
    setState(() {isLoading = false;});
    if(!responseDb.status)
    {
      scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar(responseDb.errorMessage));
      return;
    }
    scaffoldKey.currentState.showSnackBar(FWUtility.getSnackBar("Kullanıcı bilgileriniz başarıyla güncellendi."));
  }
  
  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isLoading: isLoading,
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Profil Düzenle",
            style: TextStyle(color: Color(0xff373737)),
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          elevation: 0.3,
        ),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          alignment: AlignmentDirectional.topStart,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Kullanıcı Bilgileri",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color(0xff373737),
                        fontSize: 16),
                    textAlign: TextAlign.start,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 5),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "İsim soyisim",
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                          textAlign: TextAlign.end,
                          controller: nameCont,
                          style: TextStyle(
                            color: Color(0xff717171),
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "Email Adresi",
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                          textAlign: TextAlign.end,
                          controller: emailCont,
                          style: TextStyle(
                            color: Color(0xff717171),
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Divider(
                      height: 1,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "Cep Telefonu",
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextField(
                          decoration: InputDecoration(
                            border: InputBorder.none,
                          ),
                          textAlign: TextAlign.end,
                          keyboardType: TextInputType.phone,
                          controller: phoneCont,
                          style: TextStyle(
                            color: Color(0xff717171),
                            fontSize: 14,
                          ),
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: GestureDetector(
                      child: Center(
                        child: Text(
                          "Değişiklikleri Kaydet",
                          style: TextStyle(color: Colors.redAccent),
                        ),
                      ),
                      onTap: doUpdateUser,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
